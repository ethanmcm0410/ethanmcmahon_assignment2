using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_Attack : MonoBehaviour
{
    //blade of the sword.
    public Transform overlapCirclePoint;
    //radius of the overlap circle.
    public float circleRadius;
    //will be used to attack only the enemies.
    public LayerMask enemyMask;
    //reference to the movement script.
    [SerializeField] scp_Enemy_Slime_Movement Slime_Movement;
    //reference to the slime master script.
    [SerializeField] scp_Enemy_Slime_Master slime;
    //to help stop duplication.
    [SerializeField] int counter = 0;
    //reference to the movement script
    [SerializeField] scp_Enemy_Bat_Movement bat_Movement;
    //reference to the bat master script
    [SerializeField] scp_Enemy_Bat_Master bat
       ;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void AttackLogic()
    {
        Debug.Log("method is firing");
        //returns all the colliders in the range of the circle.
        Collider2D[] enemycolliders = Physics2D.OverlapCircleAll(overlapCirclePoint.position, circleRadius, enemyMask);

        //will go through each enemy we collided with.
        foreach (Collider2D enemy in enemycolliders)
        {
            Debug.Log(enemy.name);

            if (enemy.transform.parent.GetComponent<scp_Enemy_Slime_Master>() && counter == 0)
            {
                //adds to the counter
                counter++;
                Debug.Log("Method is Firing");
                //stores a reference to the slime script
                slime = enemy.transform.parent.GetComponent<scp_Enemy_Slime_Master>();
                //gives one damage to the slime.
                slime.GetDamaged(1);
                //reference to the slime movement script.
                Slime_Movement = enemy.transform.parent.GetComponent<scp_Enemy_Slime_Movement>();
                //starts the coroutine.
                StartCoroutine(Slime_Movement.StopTheEnemyForSetAmountOfTime(2));

            }
           else if (enemy.transform.parent.GetComponent<scp_Enemy_Bat_Master>() && counter == 0)
            {
                //adds to the counter
                counter++;
                Debug.Log("Method is Firing");
                //stores a reference to the bat script
                bat = enemy.transform.parent.GetComponent<scp_Enemy_Bat_Master>();
                //gives one damage to the slime.
                bat.GetDamaged(1);
                //reference to the bat movement script.
                
                
               
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (overlapCirclePoint == null)
        {
            return;
        }

        Gizmos.DrawWireSphere(overlapCirclePoint.position, circleRadius);
    }

    void ResetCounterTOZero()
    {
        counter = 0;
    }
}
