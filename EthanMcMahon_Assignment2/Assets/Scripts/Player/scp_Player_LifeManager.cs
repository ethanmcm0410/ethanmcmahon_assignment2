using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_LifeManager : MonoBehaviour
{

    //variable to store life of the player.
    public int hp;
    //reference to the animation manager.
    scp_Player_Animation_Controller animController;
    //variable to store player status.
    public bool isPlayerAlive = true;

    private void Start()
    {
        //finds the animation controller
        animController = GetComponent<scp_Player_Animation_Controller>();
    }
    public void RemoveHp(int numberOfHpToRemove)
    {
        hp -= numberOfHpToRemove;

        if (hp <= 0)
        {
            Death();

        }
        else
        {
            //plays the hurt animation
            animController.Hurt();
        }
    }

    public void Death()
    {

        //plays the death animation.
        animController.Death();
        //records that player is dead.
        isPlayerAlive = false;
        GetComponent<Collider2D>().enabled = false;
        //makes the rigid body kinematic.
        Destroy(GetComponent<Rigidbody2D>());
        //
        GetComponent<scp_Player_Inputs>().enabled = false;
        //loads the next scene after 2 seconds.
        StartCoroutine(DeathAnimationAndThenLoadDeathScene());
    }

    IEnumerator DeathAnimationAndThenLoadDeathScene()
    {
        yield return new WaitForSeconds(2f);
        //gets the scene folder an fires the lose screen method
        FindObjectOfType<scp_Manager_Scene>().LoadDiedScreen();
    }
}
