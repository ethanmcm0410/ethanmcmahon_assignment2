using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_Animation_Controller : MonoBehaviour
{

    private Animator anim;

    //reference to the players movement script.
    private scp_Player_Movement movement;

    // Start is called before the first frame update
    void Start()
    {
        //will find the animator and store it into anim.
        anim = GetComponent<Animator>();
        //will find the script and store into the variable.
        movement = GetComponent<scp_Player_Movement>();
    }

    // Update is called once per frame
    void Update()
    {
        MovementParameter();
    }

    void MovementParameter()
    {
        //to find the absolute value of horizontal value.
        int absoluteValue = Mathf.Abs((int)movement.HorizontalValue);

        //sets the parameter to be the same as the horizontal value and converts to an integer.
        anim.SetInteger("Movement", absoluteValue);
    }

    public void AttackA()
    {
        //Fires the trigger for the attack animation.
        anim.SetTrigger("AttackA");

    }

    public void Death()
    {
        anim.SetTrigger("Death");
    }

    public void Hurt()
    {
        anim.SetTrigger("Hurt");
    }
}
