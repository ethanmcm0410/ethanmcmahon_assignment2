using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_HeavensDoor : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if i  touch the heavens door i win
        if (collision.gameObject.tag == "DoorToNextLevel")
        {
            FindObjectOfType<scp_Manager_Scene>().LoadWinScreen();
        }
    }
}
