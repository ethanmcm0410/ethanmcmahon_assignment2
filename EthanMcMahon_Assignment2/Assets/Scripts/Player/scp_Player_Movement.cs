using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_Movement : MonoBehaviour
{
   

    //reference to the rigidbody.
    private Rigidbody2D rb;

    //this will store the direction of the button press.
    public float HorizontalValue;

    //variable to control how fast the player will move.
    public float speed;

    //variable to deal with the flip.
    private Vector2 FlippedScale = new Vector2(-4f, 4);

    //variable for the correct way around.
    private Vector2 CorrectDirection = new Vector2(4f, 4);

    //reference to the players input script.
    scp_Player_Inputs inputs;
  

    
    // Start is called before the first frame update
    void Start()
    {
        //finds the rigidbody and stores it into rb.
        rb = GetComponent<Rigidbody2D>();

        //finds the inputs script.
        inputs = GetComponent<scp_Player_Inputs>();
    }

    // Update is called once per frame
    void Update()
    {
        StoreHorizontalValue();
        MovePlayer();
    }

  
    void MovePlayer()
    {
        //Creates a vector2 that we will use to give force to the player.
        Vector2 force = new Vector2(HorizontalValue * speed * Time.deltaTime, 0);

        if (rb != null)
        {
            //will add force to the rigidbody.
            rb.AddForce(force, ForceMode2D.Impulse);
        }
    }

    public void FlipThePlayer()
    {
        if (inputs.controls.Player.HorizontalMovement.ReadValue<float>() == -1)
        {
            transform.localScale = FlippedScale;
        }


        else if (inputs.controls.Player.HorizontalMovement.ReadValue<float>() == 1)
        {
            transform.localScale = CorrectDirection * 1;
        }
    }

    void StoreHorizontalValue()
    {
        //stores the current float direction.
        HorizontalValue = inputs.controls.Player.HorizontalMovement.ReadValue<float>();
    }


}