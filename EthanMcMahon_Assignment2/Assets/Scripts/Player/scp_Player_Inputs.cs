using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_Inputs : MonoBehaviour
{
    //variable that references the control scheme.
    public scp_ControlScheme controls;

    //reference to the animation controller script.
    private scp_Player_Animation_Controller animation_Controller;

    //reference to the player movement script.
    scp_Player_Movement player_Movement;

    //reference to the player attack logic
    scp_Player_Attack attackLogic;

    private void Awake()
    {
        //create a new object of type control scheme.
        controls = new scp_ControlScheme();
        //will fire this method if press a or d.
        controls.Player.HorizontalMovement.performed += _ => OnHorizontalMovement();
        //reference to the dash.
        controls.Player.Dash.performed += _ => OnDash();
        //reference to the jump.
        controls.Player.Jump.performed += _ => OnJump();
        //reference to the attack.
        controls.Player.Attack.performed += _ => OnAttack();

        //finds the animator and stores it into the variable.
        animation_Controller = GetComponent<scp_Player_Animation_Controller>();

        //will find the player movement script.
        player_Movement = GetComponent<scp_Player_Movement>();
        //will find the attack logic script.
        attackLogic = GetComponentInChildren<scp_Player_Attack>();
    }

    private void OnEnable()
    {
        //enables the controls script.
        controls.Enable();
    }

    private void OnDisable()
    {
        //disables the controls script.
        controls.Disable();
    }
    void OnHorizontalMovement()
    {
        player_Movement.FlipThePlayer();
    }

    void OnDash()
    {

    }

    void OnJump()
    {
        Debug.Log("Jump");
    }

    void OnAttack()
    {
        //fire the attack animation.
        animation_Controller.AttackA();

    }

   
}
