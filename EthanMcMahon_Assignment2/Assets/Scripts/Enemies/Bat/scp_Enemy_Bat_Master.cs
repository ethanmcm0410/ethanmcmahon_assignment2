using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Bat_Master : scp_Enemy_Master
{

    scp_Player_Attack att;
    [SerializeField] protected Animator anima;


    // Update is called once per frame
    void Update()
    {

    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Sword")
        {
            Debug.Log("Death");
            anima.SetTrigger("Death");
            Destroy(gameObject);
        }
    }

    public override void GetDamaged(int numberofLivesLost)
    {
        base.GetDamaged(numberofLivesLost);
    }
}
