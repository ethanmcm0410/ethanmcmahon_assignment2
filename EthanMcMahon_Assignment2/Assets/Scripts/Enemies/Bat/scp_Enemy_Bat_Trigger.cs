using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Bat_Trigger : MonoBehaviour
{


    public bool detection = false;

    [SerializeField] public GameObject player;
    [SerializeField] public float speed = 2.5f;
    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("player collided");
            detection = true;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
