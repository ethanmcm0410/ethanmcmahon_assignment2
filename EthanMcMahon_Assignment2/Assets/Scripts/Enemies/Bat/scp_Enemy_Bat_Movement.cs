using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Bat_Movement : MonoBehaviour
{
    //reference to the bat trigger script
    public scp_Enemy_Bat_Trigger trig;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

        

    // Update is called once per frame
    void Update()
    {
        if (trig.detection == true)
        {
            transform.position = Vector2.MoveTowards(transform.position, trig.player.transform.position, trig.speed * Time.deltaTime);
        }
    }
}
