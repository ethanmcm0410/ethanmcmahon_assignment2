using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Bat_Attack : MonoBehaviour
{

    scp_Player_LifeManager playerLife;
    [SerializeField] protected Animator anima;
    protected virtual void Start()
    {
        //finds the script and stores it as a variable.
        playerLife = FindObjectOfType<scp_Player_LifeManager>();
        anima = GetComponent<Animator>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        anima.SetTrigger("Attack");
        playerLife.GetComponent<scp_Player_LifeManager>().RemoveHp(1);  
      
    }
}
