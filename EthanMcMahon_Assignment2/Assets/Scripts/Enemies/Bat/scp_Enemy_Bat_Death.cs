using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Bat_Death : MonoBehaviour
{

    scp_Player_Attack att;
    [SerializeField] protected Animator anima;


    // Start is called before the first frame update
    protected virtual void Start()
    {
        anima = GetComponent<Animator>();
    }
        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Sword")
            {
            Debug.Log("Death");
                anima.SetTrigger("Death");
                Destroy(gameObject);
            }
        }
    



    // Update is called once per frame
    void Update()
    {
        
    }
}
