using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Slime_Waypoint_Logic : MonoBehaviour
{

    //reference to the slime movement script.
    public scp_Enemy_Slime_Movement slime_Movement;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Waypoint" && gameObject.tag == "BodyTag")
        {
            //stop
            slime_Movement.speed = 0;
           StartCoroutine(slime_Movement.FlipTheSlime());

        }
    }
}
