using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Slime_Attack : MonoBehaviour
{
    [Header("References")]
    //reference to the animation script.
    public scp_Enemy_Slime_Animation_Manager slimeAnim;
    [Header("Overlap Circle Variables")]
    //blade of the sword.
    public Transform overlapCirclePoint;
    //radius of the overlap circle.
    public float circleRadius;
    //will be used to attack only the enemies.
    public LayerMask enemyMask;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("Attacking Player");
            slimeAnim.AttackAnimation();
            //attacks the player.
            AttackLogic();
        }
    }

    public void AttackLogic()
    {
        Debug.Log("method is firing");
        //returns all the colliders in the range of the circle.
        Collider2D playercolliders = Physics2D.OverlapCircle(overlapCirclePoint.position, circleRadius, enemyMask);
        
        if (playercolliders.GetComponent<scp_Player_LifeManager>())
        {


            //removes 1 hp from the player.
            playercolliders.GetComponent<scp_Player_LifeManager>().RemoveHp(1);

        }
        





    }

    private void OnDrawGizmos()
    {
        if (overlapCirclePoint == null)
        {
            return;
        }

        Gizmos.DrawWireSphere(overlapCirclePoint.position, circleRadius);
    }
}
