using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Slime_Animation_Manager : scp_Enemy_AnimationMaster
{
    public Color damaged1;
    public Color damaged2;
    //reference to the sprite renderer.
    [SerializeField] SpriteRenderer spriteR;

    protected override void Start()
    {
        base.Start();
        //finds the sprite renderer and stores it into the variable.
        spriteR = GetComponent<SpriteRenderer>();
    }
    public void AttackAnimation()
    {
        anim.SetTrigger("Attack");
    }

    public void DeathAnimation()
    {
        anim.SetTrigger("Death");
        
    }

    public void DamagedAnimation()
    {
       anim.SetTrigger("Damaged");
    }

    public void SetMovementParameter(int movement)
    {
        anim.SetInteger("Movement", movement);

    }
    public void Damage(int damageLevel)
    {
        switch (damageLevel)
        {
            case 2:
                Debug.Log("Case 2 firing");
                spriteR.material.SetColor("_Color", damaged1);
                spriteR.color = damaged1;
                break;
            case 1:
                Debug.Log("Case 1 firing");
                spriteR.material.SetColor("_Color", damaged2);
                spriteR.color = damaged2;
                break;
        }
    }
}
