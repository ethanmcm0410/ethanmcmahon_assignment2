using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Slime_Movement : MonoBehaviour
{
    //reference to the rigidbody2d
    private Rigidbody2D SlimeRb;

    //value to control the speed of the slime.
    public float speed;

    //direction of the enemy.
    public int direction = 1;

    //variable to store the original speed.
    public float originalSpeed;

    //reference to the animation manager.
    scp_Enemy_Slime_Animation_Manager SlimeAnim;

    //bool to control if enemy can move.
    private bool canMove = true;

    // Start is called before the first frame update
    void Start()
    {
        //finds the rigidbody.
        SlimeRb = GetComponent<Rigidbody2D>();
        //finds the animation script.
        SlimeAnim = GetComponent<scp_Enemy_Slime_Animation_Manager>();
        originalSpeed = speed;

    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {


            MoveSlime();
        }
    }

    void MoveSlime()
    {
        //Creates a vector2 that we will use to give force to the player.
        Vector2 force = new Vector2(direction * speed * Time.deltaTime, 0);

        //will add force to the rigidbody.
        SlimeRb.AddForce(force, ForceMode2D.Impulse);

        //sets the movement parameter.
        SlimeAnim.SetMovementParameter((int)speed);
    }

  

    public IEnumerator FlipTheSlime()
    {
        yield return new WaitForSeconds(2f);
        //reverses the direction of movement.
        direction *= -1;

        //gives speed to the slime.
        speed = originalSpeed;
        //flips the enemy.
        transform.localScale = new Vector3(transform.localScale.x * -1, 1,1);
      
    }

    public IEnumerator StopTheEnemyForSetAmountOfTime(float time)
    {
        //will stop the movement.
        canMove = false;
        //will change the animation to idle
        speed = 0;
        //sets the movement parameter.
        SlimeAnim.SetMovementParameter((int)speed);
        yield return new WaitForSeconds(time);
        //resets the variables.
        canMove = true;
        speed = originalSpeed;
        //sets the movement parameter.
        SlimeAnim.SetMovementParameter((int)speed);
    }

    public void DeathStop()
    {
        canMove = false;
    }

    public void FlipInstantly()
    {
        //reverses the direction of movement.
        direction *= -1;
        //flips the enemy.
        transform.localScale = new Vector3(transform.localScale.x * -1, 1, 1);
        //will stop the movement.
        canMove = false;
        //will change the animation to idle
        speed = 0;
        //sets the movement parameter.
        SlimeAnim.SetMovementParameter((int)speed);
    }
}
