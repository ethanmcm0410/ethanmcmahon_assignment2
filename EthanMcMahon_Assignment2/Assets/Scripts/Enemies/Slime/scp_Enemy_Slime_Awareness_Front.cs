using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Slime_Awareness_Front : MonoBehaviour
{
    //reference to the movement script
    public scp_Enemy_Slime_Movement slime_Movement;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Debug.Log("I can see the player");
            //attack

            //if the enemy leaves the range, speed up for a bit

        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //speed up for a bit
            StartCoroutine(SpeedUpForASetAmountOfTime(3));
        }
    }
    IEnumerator SpeedUpForASetAmountOfTime(float time)
    {
        slime_Movement.speed = 60;
        yield return new WaitForSeconds(time);
        slime_Movement.speed = slime_Movement.originalSpeed;
    }
}
