using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Slime_Awareness_Back : MonoBehaviour
{
    //reference to the move script.
    public scp_Enemy_Slime_Movement slime_Movement;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            //Flip me
            slime_Movement.FlipInstantly();
        }
    }
}
