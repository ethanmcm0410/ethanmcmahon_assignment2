using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Slime_Master : scp_Enemy_Master
{
    //reference to the slime animation script.
    scp_Enemy_Slime_Animation_Manager slimeAnimation;
    //reference to the slime movement script.
    scp_Enemy_Slime_Movement slime_Movement;
    //deactivates the attack script.



    protected override void Start()
    {
        base.Start();
        //will find the animation script.
        slimeAnimation = GetComponent<scp_Enemy_Slime_Animation_Manager>();
        //will find the movement script and will store into the variable.
        slime_Movement = GetComponent<scp_Enemy_Slime_Movement>();
    }


    public override void Death()
    {
        if (lives <= 0)
        {
            base.Death();
            slimeAnimation.DeathAnimation();
            //Deactivates the movement script.
            GetComponent<scp_Enemy_Slime_Movement>().enabled = false;
            Invoke("DeactivateAnimationScript", 0.3f);
            //deactivates the attack script
            GetComponentInChildren<scp_Enemy_Slime_Attack>().enabled = false;
            //deactivates the collider
            GetComponentInChildren<CircleCollider2D>().enabled = false;
            //deactivates the back awareness script
            Destroy(GetComponentInChildren<scp_Enemy_Slime_Awareness_Back>());
            
        }

    }


    public override void GetDamaged(int numberofLivesLost)
    {
        base.GetDamaged(numberofLivesLost);

        //if lives > 0 will fire the get damaged animation.
        if (lives > 0)
        {
            slimeAnimation.DamagedAnimation();
            //changes the color based on the damage.
            slimeAnimation.Damage(lives);

        }
        else
        {
            //else will fire the death().
            Death();
            //will stop the enemy from moving.
            slime_Movement.DeathStop();
        }


    }

    void DeactivateAnimationScript()
    {
        //Deactivates the enemy animation script.
        GetComponent<scp_Enemy_Slime_Animation_Manager>().enabled = false;

    }
   
}
