using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_AnimationMaster : MonoBehaviour
{
    //reference to the animator.
    [SerializeField] protected Animator anim;


    // Start is called before the first frame update
    protected virtual void Start()
    {
        //will find the animator and will store it in anim.
        anim = GetComponent<Animator>();
    }

}
