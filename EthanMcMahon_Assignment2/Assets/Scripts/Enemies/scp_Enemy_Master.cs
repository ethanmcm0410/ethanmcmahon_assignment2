using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Master : MonoBehaviour
{
    //enemy lives
    public int lives;

    //will be the sprite that we use for the enemy.
    public Sprite enemySprite;

    //will be used to define the name of the enemy.
    public string enemyName;

    //reference to the game manager script.
    protected scp_Manager_Game gm;


    protected virtual void Start()
    {
        //finds the gm script.
        gm = FindObjectOfType<scp_Manager_Game>();
    }


    public virtual void Death()
    {
        if (lives <= 0)
        {
            gm.EnemyKilledPlusOne();
        }
    }

    public virtual void GetDamaged(int numberofLivesLost)
    {
        //removes a set amount of lives.
        lives -= numberofLivesLost;

        

    }

    protected void SelfDestruct(float timeBeforeGettingDestroyed)
    {
        //destroys the enemy after a set amount of time.
        Destroy(this.gameObject, timeBeforeGettingDestroyed);
    }
}
