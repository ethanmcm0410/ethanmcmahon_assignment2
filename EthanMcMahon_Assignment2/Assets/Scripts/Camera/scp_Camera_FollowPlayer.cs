using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Camera_FollowPlayer : MonoBehaviour
{

    //variable to store the target of the camera.
    public Transform target;

    //variable to store offset to the camera.
    public Vector3 CameraOffset;

    //variable that will be used by the lerp.
    public float smoothSpeed = 0.125f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CameraFollow();
    }


    private void FixedUpdate()
    {
        CameraFollow();
    }
    void CameraFollow()
    {

        //this is going to store the desired position.
        Vector3 desiredPosition = target.position + CameraOffset;
        //will lerp the position.
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);

        //will follow player.
        transform.position = smoothedPosition;
    }
}
