using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Manager_Game : MonoBehaviour
{
    //variable to keep track of how many enemies we have killed.
    public int enemiesKilled = 0;

    public static scp_Manager_Game gameManagerinstance;
    private void Awake()
    {
        if (gameManagerinstance != null)
        {
            Destroy(this.gameObject);
        }
        else if (gameManagerinstance == null)
        {
            gameManagerinstance = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }

    public void EnemyKilledPlusOne()
    {
        //will add one to the enemies killed variable.
        enemiesKilled++;

    }
}
