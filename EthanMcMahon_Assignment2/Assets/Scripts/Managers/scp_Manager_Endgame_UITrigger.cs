using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Manager_Endgame_UITrigger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //should fire the right thing at start.
        FindObjectOfType<scp_Manager_UI>().LocateAndModifyEnemyKilledText();
    }
}
