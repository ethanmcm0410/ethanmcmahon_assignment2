using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scp_Manager_UI : MonoBehaviour
{
    [Header("player's Variables")]
    //slider that will represent player hp.
    public Slider playerHpSlider;

    //reference to the player life script.
    scp_Player_LifeManager playerLife;

    [Header("Score Variables")]
    public Text enemiesKilledText;

    //reference to the game manager.
    private scp_Manager_Game gm;

    [Header("Death Screen Variables")]
    [SerializeField] GameObject enemyKilledTextObject;
    [SerializeField] Text enemyKilledTextComponent;

    public static scp_Manager_UI uiInstance;

    private void Awake()
    {
        if (uiInstance != null)
        {
            Destroy(this.gameObject);
        }
        else if (uiInstance == null)
        {
            uiInstance = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        //finds the script and stores it into the variable.
        playerLife = FindObjectOfType<scp_Player_LifeManager>();

        //will make the max value of the slider to be the same as the maximum hp of the player.
        playerHpSlider.maxValue = playerLife.hp;
        //showcase the current life.
        playerHpSlider.value = playerLife.hp;


        //finds the script and fills the variables with it.
        gm = FindObjectOfType<scp_Manager_Game>();
        //will make the enemies killed text be the same as the score.
        enemiesKilledText.text = gm.enemiesKilled.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        //makes the slider ui equal at the current HP.
        playerHpSlider.value = playerLife.hp;
        //will make the enemies killed text be the same as the score.
        enemiesKilledText.text = gm.enemiesKilled.ToString();

    }

    public void LocateAndModifyEnemyKilledText()
    {
        //will find the enemy killed game object.
        enemyKilledTextObject = GameObject.Find("EnemyKilledNumber");
        //will get the text component.
        enemyKilledTextComponent = enemyKilledTextObject.GetComponent<Text>();
        //Updates the count.
        enemyKilledTextComponent.text = gm.enemiesKilled.ToString();


    }

}
