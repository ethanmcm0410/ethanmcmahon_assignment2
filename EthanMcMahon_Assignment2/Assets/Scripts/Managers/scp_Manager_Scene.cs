using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scp_Manager_Scene : MonoBehaviour
{
    public static scp_Manager_Scene scneManager;

    private void Awake()
    {
        if (scneManager != null)
        {
            Destroy(this.gameObject);
        }
        else if(scneManager == null)
        {
            scneManager = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }
    public void LoadNextScene()
    {
        //will load the next scene in the index.
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadWinScreen()
    {
        SceneManager.LoadScene("Scn_4_YouWon");
    }

    public void LoadDiedScreen()
    {
        SceneManager.LoadScene("Scn_3_YouDied");
    }
}
